from datetime import timedelta

from app import get_apple, insert_apple

import airflow
from airflow import DAG
from airflow.operators.python_operator import PythonOperator

# These args will get passed on to each operator
# You can override them on a per-task basis during operator initialization
default_args = {
    'owner': 'airflow',
    'id': '1',
    'start_date': airflow.utils.dates.days_ago(1)
}

dag = DAG(
    'practice',
    default_args=default_args,
    description='A company project',
    schedule_interval=timedelta(minutes=5),
)

t1 = PythonOperator(
    task_id='get',
    python_callable=get_apple,
    dag=dag
)

t2 = PythonOperator(
    task_id='insert',
    python_callable=insert_apple,
    dag=dag
)
t1 >> t2