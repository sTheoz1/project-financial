import requests
import pymongo 
from datetime import datetime
import pprint
import json


def get_apple():
    res_profile = requests.get(
        "https://financialmodelingprep.com/api/v3/profile/AAPL?apikey=743762e977b7b73e968109ce970a35bc")
    profile_apple = res_profile.json()
    res_rating = requests.get(
        "https://financialmodelingprep.com/api/v3/rating/AAPL?apikey=743762e977b7b73e968109ce970a35bc")
    rating_apple = res_rating.json()
    data = {"rating": rating_apple, "profile": profile_apple, 'timestamp': str(datetime.now().timestamp())}
    with open("/tmp/result.json", "w") as file:
        json.dump(data, file)


def insert_apple():
    with open("/tmp/result.json") as file:
        conn = pymongo.MongoClient("mongodb://root:example@mongo:27017/")
        mydb = conn["company"]
        collection = mydb["apple"]
        print("Bonjour")
        data = json.load(file)
        print(data)
        id = collection.insert_one(data).inserted_id

        print(id)
        print(mydb.list_collection_names())

        pprint.pprint(collection.find_one({"symbol": "AAPL"}))